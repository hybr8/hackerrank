﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

namespace DynamicArray
{
    class Result
    {

        /*
         * Complete the 'dynamicArray' function below.
         *
         * The function is expected to return an INTEGER_ARRAY.
         * The function accepts following parameters:
         *  1. INTEGER n
         *  2. 2D_INTEGER_ARRAY queries
         */

        public static List<int> dynamicArray(int n, List<List<int>> queries)
        {
            List<int> res = new List<int>();
            int lastAnswer = 0;
            List<List<int>> seqList = new List<List<int>>();
            for (int i = 0; i < n; i++)
            {
                List<int> seq = new List<int>();
                seqList.Add(seq);
            }

            foreach (var query in queries)
            {
                List<int> seq = seqList[((query[1] ^ lastAnswer) % n)];
                if (query[0] == 1)
                {
                    seq.Add(query[2]);
                }
                else
                {
                    lastAnswer = seq[query[2] % seq.Count];
                    res.Add(lastAnswer);
                }
            }

            return res;
        }
    }

    class Solution
    {
        public static void Main(string[] args)
        {
            string outputPath;
            try
            {
                outputPath = @System.Environment.GetEnvironmentVariable("OUTPUT_PATH") ?? GetOutPutPath();
            }
            catch
            {
                outputPath = GetOutPutPath();
            }
            TextWriter textWriter = new StreamWriter(outputPath, true);

            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int q = Convert.ToInt32(firstMultipleInput[1]);

            List<List<int>> queries = new List<List<int>>();

            for (int i = 0; i < q; i++)
            {
                queries.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(queriesTemp => Convert.ToInt32(queriesTemp)).ToList());
            }

            List<int> result = Result.dynamicArray(n, queries);

            textWriter.WriteLine(String.Join("\n", result));

            textWriter.Flush();
            textWriter.Close();
        }

        private static string GetOutPutPath()
        {
            return Path.Combine(Directory.GetCurrentDirectory(), "output.txt");
        }
    }

}
